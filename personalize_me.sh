#!/bin/bash


#
# generate a dhparam file for NGINX
# BEWARE: this takes a long time
#
openssl dhparam -out /srv/persistent-data/docker-scripts/nginx/ssl/dhparam.pem 4096 

#
# add a new self-signed cert to the NGINX config
#
REALNAME=`hostname`
openssl req \
    -new \
    -newkey rsa:4096 \
    -days 365 \
    -nodes \
    -x509 \
    -subj "/C=US/ST=NC/L=Durham/O=Duke_University_OIT/CN=$REALNAME" \
    -keyout /srv/persistent-data/docker-scripts/nginx/ssl/server.key \
    -out /srv/persistent-data/docker-scripts/nginx/ssl/server.crt

#
# generate a mapping file so we have unique passwords for the docker users
#
rm /srv/persistent-data/docker-scripts/mapping
for INDEX in 0 1 2 3 4 
do
  USERID=`printf "%03d" $INDEX`
  PORT=40$USERID
  echo $USERID,$PORT,`openssl rand -base64 60 | tr -dc _A-Z-a-z-0-9 | cut -c1-30`  >> /srv/persistent-data/docker-scripts/mapping
done 


#
# append the login info for the web site to the README by looping through the user mappings
#
sudo rm ~rapiduser/README
cp /srv/persistent-data/src/README-template ~rapiduser/README
INPUT=/srv/persistent-data/docker-scripts/mapping
OLDIFS=$IFS
IFS=,
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read usernum port pw
do
  echo https://$REALNAME\:$port password: $pw home directory: /srv/persistent-data/homedirs/user$usernum >> ~rapiduser/README	
done < $INPUT
IFS=$OLDIFS
sudo chown  rapiduser:rapiduser ~rapiduser/README


#
# return an exit code
#
exit 0


