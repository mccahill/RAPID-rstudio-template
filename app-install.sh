#!/bin/bash

# Install Docker, NGINX, Docker-gen, RStudio

# We assume that you do a git clone of the repository, cd to the
# directory and run this script from there
#
# where are we being run from?
#
INSTALLFROM=~rapiduser/src

#
# install the docker daemon
#
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

sudo apt-get update
sudo apt-get install -y docker-ce
sudo apt-get install -y csh
sudo apt-get install -y net-tools
sudo apt autoremove -y


#
# install directories to hold docker scripts and persistent home directories
#
sudo chmod go+w /srv
mkdir /srv/persistent-data
mkdir /srv/persistent-data/src
mkdir /srv/persistent-data/homedirs
mkdir /srv/persistent-data/docker-scripts

#
#  create home directories for the docker container users 
#
for INDEX in 0 1 2 3 4
do
  USERID=`printf "%03d" $INDEX`
  cp -r $INSTALLFROM/homedirs/user-template /srv/persistent-data/homedirs/user$USERID
done


#
# get the NGINX container source and compile it
#
cd /srv/persistent-data/src
git clone http://gitlab.oit.duke.edu/mccahill/docker-nginx.git
cd /srv/persistent-data/src/docker-nginx
git pull origin duke
./build


#
# get the Docker-gen container source and compile it
#
cd /srv/persistent-data/src
git clone http://gitlab.oit.duke.edu/mccahill/docker-gen.git
cd /srv/persistent-data/src/docker-gen
./build


#
# get the RStudio container source and compile it
#
cd /srv/persistent-data/src
git clone https://mccahill@github.com/mccahill/docker-rstudio.git
cd /srv/persistent-data/src/docker-rstudio
./build


#
# copy docker start/stop scripts into place
#
cp -r $INSTALLFROM/docker-scripts/* /srv/persistent-data/docker-scripts/


#
# write out the README files in the homedirectory for the rapiduser
#
cp $INSTALLFROM/README-how-to-build-Rstudio ~rapiduser/
cp $INSTALLFROM/README-template ~rapiduser/README


#
# personalize this instance
#
sudo /usr/local/bin/personalize_me


#
# start the docker containers for the application
#
cd /srv/persistent-data/docker-scripts
./run-everything


