#!/bin/bash

# Install Docker, NGINX, Docker-gen, RStudio

# this script assumes that an ansible playbook did swome iniitial setup and is now calling us
# if you want to install without the ansible initial setup, usre the app-install.sh script instead
#
# where are we being run from?
#
INSTALLFROM=/srv/persistent-data/src

#
# copy the app-specific personalize_me script into place
# this will be run by RAPID when a new instance of the VM is
# cloned from the vmware template 
#
sudo cp $INSTALLFROM/personalize_me.sh /usr/local/bin/personalize_me
sudo chmod ugo+x /usr/local/bin/personalize_me

#
#  create home directories for the docker container users 
#
for INDEX in 0 1 2 3 4
do
  USERID=`printf "%03d" $INDEX`
  cp -r $INSTALLFROM/homedirs/user-template /srv/persistent-data/homedirs/user$USERID
done


#
# get the NGINX container source 
#
cd /srv/persistent-data/src
git clone http://gitlab.oit.duke.edu/mccahill/docker-nginx.git
cd /srv/persistent-data/src/docker-nginx
git pull origin duke
# ./build


#
# get the Docker-gen container source 
#
cd /srv/persistent-data/src
git clone http://gitlab.oit.duke.edu/mccahill/docker-gen.git
cd /srv/persistent-data/src/docker-gen
# ./build


#
# get the RStudio container source 
#
cd /srv/persistent-data/src
git clone https://mccahill@github.com/mccahill/docker-rstudio.git
cd /srv/persistent-data/src/docker-rstudio
# ./build

#
# fetch the docker container images
#
mkdir /srv/persistent-data/docker-images
cd /srv/persistent-data/docker-images
wget https://people.duke.edu/~mccahill/RAPID-docker-gen.tar
wget https://people.duke.edu/~mccahill/RAPID-nginx.tar
wget https://people.duke.edu/~mccahill/RAPID-r-studio.tar

#
# load the images
#
sudo docker load -i ./RAPID-docker-gen.tar
sudo docker load -i ./RAPID-nginx.tar
sudo docker load -i ./RAPID-r-studio.tar

#
# remove the tar files to save some space
#
rm -rf /srv/persistent-data/docker-images

#
# copy docker start/stop scripts into place
#
cp -r $INSTALLFROM/docker-scripts/* /srv/persistent-data/docker-scripts/


#
# write out the README files in the homedirectory for the rapiduser
#
cp $INSTALLFROM/README-how-to-build-Rstudio ~rapiduser/
cp $INSTALLFROM/README-template ~rapiduser/README


#
# personalize this instance
#
sudo /usr/local/bin/personalize_me


#
# leave a flag saying we got done
#
sudo cp /dev/null /srv/persistent-data/docker-scripts/app-install-done

#
# start the docker containers for the application
#
# cd /srv/persistent-data/docker-scripts
# ./run-everything


