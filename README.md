Source for creating a RAPID virtual machine template
that includes Docker, RStudio, NGINX, and Docker-gen.

The app-install.sh script will install the applications
(docker, rstudio, etc.), and create home directories
for the RStudio users in

     /srv/persistent-data/homedirs

The personalize_me.sh script is intended to be run on
instances created from the template - this is where we
do things like generate new SSL site certificates, etc.
that have to be unique to the instance. We also change
the server name here to match its entry in the DNS. This
install puts the script into the place where RAPID will run
it: 

     /usr/local/bin/personalize_me	 

Assumptions:

1.) You want to keep all the important app-specific stuff in 
     /srv/persistent-data
We do this to keep a clear separation between the operating
system, and the docker container apps and user data connected
with those apps. If you are going to back up something, make
sure you back up the /srv/persistent-data heirarchy.

2.) Users may want to modify the build of RStudio or the 
other components (perhaps to include different libraries) 
so the source for the docker components is included - look
in /srv/persistent-data/src

